//
//  Generic.swift
//  SwiftParsing
//
//  Created by Janek Spaderna on 09.05.15.
//  Copyright (c) 2015 Janek Spaderna. All rights reserved.
//

import Foundation
import SwiftBase
import SwiftParsing

public enum GenCharParser<U> {
    public typealias T = Parser<Character, U, Character>
}

public func oneOf<U>(set: NSCharacterSet) -> GenCharParser<U>.T {
    return satisfy { [set = set.copy() as! NSCharacterSet] in $0.inCharacterSet(set) }
}

public func oneOf<U>(set: Set<Character>) -> GenCharParser<U>.T {
    return satisfy(set.contains)
}

public func oneOf<U>(str: String) -> GenCharParser<U>.T {
    return satisfy(str.characters.contains)
}

public func oneOf<U, I: IntervalType where I.Bound == Character>(range: I) -> GenCharParser<U>.T {
    return satisfy(range.contains)
}

public func noneOf<U>(set: NSCharacterSet) -> GenCharParser<U>.T {
    return satisfy { [set = set.copy() as! NSCharacterSet] in !$0.inCharacterSet(set) }
}

public func noneOf<U>(set: Set<Character>) -> GenCharParser<U>.T {
    return satisfy((!) • set.contains)
}

public func noneOf<U>(str: String) -> GenCharParser<U>.T {
    return satisfy((!) • str.characters.contains)
}

public func noneOf<U, I: IntervalType where I.Bound == Character>(range: I) -> GenCharParser<U>.T {
    return satisfy((!) • range.contains)
}

public func char<U>(c: Character) -> GenCharParser<U>.T {
    return satisfy((==) « c) <?> "'\(c)'"
}

public func satisfy<U>(f: Character -> Bool) -> GenCharParser<U>.T {
    return tokenPrim({ "'\($0)'" }, nextPos: uncurry(SourcePos.moveWith)) { f($0) ? .Some($0) : .None }
}

public func string<U>(s: String) -> Parser<Character, U, String> {
    return s.characters.tokens(
        showTokens: { "\"\(String($0))\"" },
        advance: uncurry(SourcePos.moveWith)) <^> String.init
}
