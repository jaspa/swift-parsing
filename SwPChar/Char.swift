//
//  Char.swift
//  SwiftParsing
//
//  Created by Janek Spaderna on 08.05.15.
//  Copyright (c) 2015 Janek Spaderna. All rights reserved.
//

import Foundation
import SwiftBase
import SwiftParsing

public func spaces<U>() -> Parser<Character, U, Void> {
    return space().skipMany
}

public func space<U>() -> GenCharParser<U>.T {
    return oneOf(NSCharacterSet.whitespaceCharacterSet()) <?> "whitespace"
}

public func newline<U>() -> GenCharParser<U>.T {
    return char("\n") <?> "lf newline"
}

public func crlf<U>() -> GenCharParser<U>.T {
    return char("\r") >>- { _ in char("\n") } <?> "crlf newline"
}

public func endOfLine<U>() -> GenCharParser<U>.T {
    return newline() <|> crlf() <?> "newline"
}

public func tab<U>() -> GenCharParser<U>.T {
    return char("\t") <?> "tab"
}

public func upper<U>() -> GenCharParser<U>.T {
    return oneOf(NSCharacterSet.uppercaseLetterCharacterSet()) <?> "uppercase letter"
}

public func lower<U>() -> GenCharParser<U>.T {
    return oneOf(NSCharacterSet.lowercaseLetterCharacterSet()) <?> "lowercase letter"
}

public func alphaNum<U>() -> GenCharParser<U>.T {
    return oneOf(NSCharacterSet.alphanumericCharacterSet()) <?> "letter or digit"
}

public func letter<U>() -> GenCharParser<U>.T {
    return oneOf(NSCharacterSet.letterCharacterSet()) <?> "letter"
}

public func digit<U>() -> GenCharParser<U>.T {
    return oneOf(NSCharacterSet.decimalDigitCharacterSet()) <?> "digit"
}

public func any<U>() -> GenCharParser<U>.T {
    return item(uncurry(SourcePos.moveWith))
}
