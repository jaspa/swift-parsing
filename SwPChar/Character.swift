//
//  Character.swift
//  SwiftParsing
//
//  Created by Janek Spaderna on 08.05.15.
//  Copyright (c) 2015 Janek Spaderna. All rights reserved.
//

import Foundation
import SwiftBase

public extension Character {
    private var unicodeScalars: String.UnicodeScalarView {
        return String(self).unicodeScalars
    }

    func inCharacterSet(set: NSCharacterSet) -> Bool {
        let (first, others) = unicodeScalars.head
        return set.unicodeScalarIsMember(first) && others.all(NSCharacterSet.nonBaseCharacterSet().unicodeScalarIsMember)
    }
}

private extension NSCharacterSet {
    func unicodeScalarIsMember(scalar: UnicodeScalar) -> Bool {
        return longCharacterIsMember(scalar.value)
    }
}
