//
//  StateSwitch.swift
//  SwiftParsing
//
//  Created by Janek Spaderna on 03.05.15.
//  Copyright (c) 2015 Janek Spaderna. All rights reserved.
//

import Foundation
import SwiftBase

func switchState<T, U, A, B>() -> StateSwitch<T, U, A, Consumed<Reply<T, U, B>>> {
    return StateSwitch()
}

func switchLiftOk<T, U, A>() -> StateSwitch<T, U, A, Consumed<Reply<T, U, A>>> {
    return StateSwitch()
        .emptyOk(Consumed.Empty • Reply.Ok)
        .eatenOk(Consumed.Eaten • Reply.Ok)
}

func switchLiftErrors<T, U, A, B>() -> StateSwitch<T, U, A, Consumed<Reply<T, U, B>>> {
    return StateSwitch()
        .emptyError(Consumed.Empty • Reply.Error)
        .eatenError(Consumed.Eaten • Reply.Error)
}

func switchLiftEmpty<T, U, A>() -> StateSwitch<T, U, A, Consumed<Reply<T, U, A>>> {
    return StateSwitch()
        .emptyOk(Consumed.Empty • Reply.Ok)
        .emptyError(Consumed.Empty • Reply.Error)
}

func switchLiftEaten<T, U, A>() -> StateSwitch<T, U, A, Consumed<Reply<T, U, A>>> {
    return StateSwitch()
        .eatenOk(Consumed.Eaten • Reply.Ok)
        .eatenError(Consumed.Eaten • Reply.Error)
}

struct StateSwitch<T, U, A, R> {
    typealias OkFunction = (A, State<T, U>, ParseError) -> R
    typealias ErrorFunction = ParseError -> R

    private var onEatenOk: OkFunction!, onEatenError: ErrorFunction!
    private var onEmptyOk: OkFunction!, onEmptyError: ErrorFunction!

    init() {}

    func ok(f: OkFunction) -> StateSwitch {
        return m { (inout n: StateSwitch) in n.onEmptyOk = f; n.onEatenOk = f }
    }

    func error(f: ErrorFunction) -> StateSwitch {
        return m { (inout n: StateSwitch) in n.onEmptyError = f; n.onEatenError = f }
    }

    func eatenOk(f: OkFunction) -> StateSwitch {
        return m { (inout n: StateSwitch) in n.onEatenOk = f }
    }

    func eatenError(f: ErrorFunction) -> StateSwitch {
        return m { (inout n: StateSwitch) in n.onEatenError = f }
    }

    func emptyOk(f: OkFunction) -> StateSwitch {
        return m { (inout n: StateSwitch) in n.onEmptyOk = f }
    }

    func emptyError(f: ErrorFunction) -> StateSwitch {
        return m { (inout n: StateSwitch) in n.onEmptyError = f }
    }

    func switchOn(s: Consumed<Reply<T, U, A>>) -> R {
        switch s {
        case .Eaten(let .Ok(v, s, e)): return onEatenOk(v, s, e)
        case .Eaten(let .Error(e)):    return onEatenError(e)
        case .Empty(let .Ok(v, s, e)): return onEmptyOk(v, s, e)
        case .Empty(let .Error(e)):    return onEmptyError(e)
        }
    }

    private func m(f: (inout StateSwitch) -> Void) -> StateSwitch {
        var new = self
        f(&new)
        return new
    }
}
