//
//  Parser.swift
//  SwiftParsing
//
//  Created by Janek Spaderna on 20.04.15.
//  Copyright (c) 2015 Janek Spaderna. All rights reserved.
//

import Foundation
import SwiftBase

public struct Parser<T, U, A> {
    typealias Result = Consumed<Reply<T, U, A>>
    let parse: State<T, U> -> Result

    init(parse: State<T, U> -> Result) {
        self.parse = parse
    }

    public func run<C: CollectionType where C.Generator.Element == T>(list: C, source: String, user: U) -> Either<ParseError, (A, AnyGenerator<T>)> {
        let state = State(input: list, position: SourcePos(source: source), user: user)
        switch parse(state).value {
            case let .Ok(val, s, _): return .Right(val, anyGenerator(s.input))
            case let .Error(error):  return .Left(error)
        }
    }
}

// MARK: - Functor

public extension Parser {
    func map<B>(f: A -> B) -> Parser<T, U, B> {
        return Parser<T, U, B> { s in self.parse(s) <^> f }
    }
}

public func <^><T, U, A, B>(p: Parser<T, U, A>, f: A -> B) -> Parser<T, U, B> {
    return p.map(f)
}

// MARK: - Monad

public extension Parser {
    init(pure x: A) {
        parse = { .Empty(.Ok(x, $0, ParseError(position: $0.position))) }
    }

    init(fail msg: String) {
        parse = { .Empty(.Error(ParseError(position: $0.position, messages: [.Message(msg)]))) }
    }

    init(unexpected msg: String) {
        parse = { .Empty(.Error(ParseError(position: $0.position, messages: [.Unexpected(msg)]))) }
    }

    func flatMap<B>(f: A -> Parser<T, U, B>) -> Parser<T, U, B> {
        return Parser<T, U, B> { s in
            switchLiftErrors()
                .eatenOk { x, s, err in
                    switchLiftEaten()
                        .emptyOk { .Eaten(.Ok($0, $1, err.mergeWith($2))) }
                        .emptyError { .Eaten(.Error(err.mergeWith($0))) }
                        .switchOn(f(x).parse(s))
                }.emptyOk { x, s, err in
                    switchLiftEaten()
                        .emptyOk { .Empty(.Ok($0, $1, err.mergeWith($2))) }
                        .emptyError { .Empty(.Error(err.mergeWith($0))) }
                        .switchOn(f(x).parse(s))
                }.switchOn(self.parse(s))
        }
    }
}

public func flatten<T, U, A>(p: Parser<T, U, Parser<T, U, A>>) -> Parser<T, U, A> {
    return p.flatMap(id)
}

public func pure<T, U, A>(x: A) -> Parser<T, U, A> {
    return Parser(pure: x)
}

public func >>-<T, U, A, B>(p: Parser<T, U, A>, f: A -> Parser<T, U, B>) -> Parser<T, U, B> {
    return p.flatMap(f)
}

// A precedence as high as the one of (>>-)
infix operator >>> { associativity left precedence 103 }

public func >>><T, U, A, B>(p: Parser<T, U, A>, @autoclosure(escaping) q: () -> Parser<T, U, B>) -> Parser<T, U, B> {
    // Don't use `const` here, as this would defeat the `@autoclosure`-laziness
    // At least I think we want laziness here...
    return p.flatMap { _ in q() }
}

// MARK: - MonadPlus / Alternative

public extension Parser {
    init() {
        self.parse = { .Empty(.Error(ParseError(position: $0.position))) }
    }

    func choice(p2: Parser) -> Parser {
        return Parser { s in
            switchLiftEaten()
                .emptyOk { .Empty(.Ok($0, $1, $2)) }
                .emptyError { err in
                    switchLiftEaten()
                        .emptyOk { .Empty(.Ok($0, $1, err.mergeWith($2))) }
                        .emptyError { .Empty(.Error(err.mergeWith($0))) }
                        .switchOn(p2.parse(s))
                }.switchOn(self.parse(s))
        }
    }
}

// A precedence only a little bit higher than assignment (beeing 90)
infix operator <|> { associativity right precedence 92 }

public func <|><T, U, A>(p1: Parser<T, U, A>, p2: Parser<T, U, A>) -> Parser<T, U, A> {
    return p1.choice(p2)
}

// MARK: - Labeling

public extension Parser {
    func label(msg: String) -> Parser {
        return label([msg])
    }

    func label(msgs: [String]) -> Parser {
        func setExpectErrors(error: ParseError, _ msgs: [String]) -> ParseError {
            if msgs.isEmpty {
                return error.setMessage(.Expected(""))
            }

            let (msg, msgs) = msgs.head
            return msgs.reduce(error.setMessage(.Expected(msg))) { $0.addMessage(.Expected($1)) }
        }

        return Parser { s in
            switchLiftEaten()
                .emptyOk { .Empty(.Ok($0, $1, $2.isUnknown ? $2 : setExpectErrors($2, msgs))) }
                .emptyError { .Empty(.Error(setExpectErrors($0, msgs))) }
                .switchOn(self.parse(s))
        }
    }
}

infix operator <?> { precedence 0 }

public func <?><T, U, A>(p: Parser<T, U, A>, msg: String) -> Parser<T, U, A> {
    return p.label(msg)
}
