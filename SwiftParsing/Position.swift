//
//  Position.swift
//  SwiftParsing
//
//  Created by Janek Spaderna on 26.04.15.
//  Copyright (c) 2015 Janek Spaderna. All rights reserved.
//

import Foundation
import SwiftBase

public struct SourcePos {
    public let source: String
    public let line: Int
    public let char: Int

    public init(source: String, line: Int, char: Int) {
        self.source = source
        self.line = line
        self.char = char
    }

    public init(source: String) {
        self.source = source
        self.line = 1
        self.char = 1
    }
}

public extension SourcePos {
    func moveToLine(line: Int, cr: Bool = false) -> SourcePos {
        return SourcePos(source: source, line: line, char: cr ? 1 : char) }
    func moveToChar(char: Int) -> SourcePos {
        return SourcePos(source: source, line: line, char: char) }

    func increaseLine(n: Int = 1, cr: Bool = false) -> SourcePos {
        return SourcePos(source: source, line: line + n, char: cr ? 1 : char) }
    func increaseChar(n: Int = 1) -> SourcePos {
        return SourcePos(source: source, line: line, char: char + n) }

    func moveWith(c: Character) -> SourcePos {
        switch c {
            case "\n": return increaseLine(cr: true)
            case "\t": return increaseChar(8 - (char - 1) % 8)
            default:   return increaseChar()
        }
    }

    func moveWith<S: SequenceType where S.Generator.Element == Character>(sequence: S) -> SourcePos {
        return sequence.reduce(self, combine: SourcePos.moveWith » uncurry)
    }

    func moveWith<G: GeneratorType where G.Element == Character>(generator: G) -> SourcePos {
        return GeneratorSequence(generator).reduce(self, combine: SourcePos.moveWith » uncurry)
    }
}

extension SourcePos: CustomStringConvertible {
    public var description: String {
        let pos = "(line \(line), column \(char))"
        return source.isEmpty ? pos : "\"\(source)\" " + pos
    }
}

extension SourcePos: Comparable {
}

public func ==(lhs: SourcePos, rhs: SourcePos) -> Bool {
    return lhs.char == rhs.char && lhs.line == rhs.line && lhs.source == rhs.source
}

public func <(lhs: SourcePos, rhs: SourcePos) -> Bool {
    return lhs.char < rhs.char && lhs.line < rhs.line && lhs.source < rhs.source
}
