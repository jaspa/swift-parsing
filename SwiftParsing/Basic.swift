//
//  Basic.swift
//  SwiftParsing
//
//  Created by Janek Spaderna on 02.05.15.
//  Copyright (c) 2015 Janek Spaderna. All rights reserved.
//

import Foundation
import SwiftBase

public func item<U, A>(advance: (SourcePos, A) -> SourcePos) -> Parser<A, U, A> {
    return tokenPrim(nextPos: advance, transform: pure)
}

public func tokenPrim<T, U, A>(show: T -> String = mkString, nextPos: (SourcePos, T) -> SourcePos, transform: T -> A?) -> Parser<T, U, A> {
    return Parser { (var s) in
        guard let c = s.input.next() else { return .Empty(makeUnexpectedError(s.position)) }
        guard let x = transform(c) else { return .Empty(makeUnexpectedError(s.position, show(c))) }

        s.position = nextPos(s.position, c)
        return .Eaten(.Ok(x, s, ParseError(position: s.position)))
    }
}

public extension CollectionType where Generator.Element: Equatable {

    func tokens<U>(
        showTokens show: [Generator.Element] -> String = mkString,
        advance: (SourcePos, Self) -> SourcePos
    ) -> Parser<Generator.Element, U, Self> {

        if isEmpty {
            return Parser(pure: self)
        }

        return Parser { (var s) in
            var first = true
            var generator = self.generate(), genOld = generator

            func makeError(msg: String) -> Consumed<Reply<Generator.Element, U, Self>> {
                let expected = show(Array(GeneratorSequence(genOld)))
                let error = ParseError(position: s.position, messages: [.SysUnexpected(msg), .Expected(expected)])
                return first ? .Empty(.Error(error)) : .Eaten(.Error(error))
            }

            while let t = generator.next() {
                guard let x = s.input.next() else { return makeError("") }
                if t != x { return makeError(show([x])) }
                first = false
                genOld = generator
            }

            s.position = advance(s.position, self)
            return .Eaten(.Ok(self, s, ParseError(position: s.position)))
        }
    }
}

public extension Parser {
    var clean: Parser {
        return Parser { s in
            switchLiftOk()
                .error(Consumed.Empty • Reply.Error)
                .switchOn(self.parse(s))
        }
    }

    var dirty: Parser {
        return Parser { s in
            switchLiftErrors()
                .ok(Consumed.Eaten • Reply.Ok)
                .switchOn(self.parse(s))
        }
    }

    var lookAhead: Parser {
        return Parser { s in
            switchLiftErrors()
                .ok { x, _, _ in .Empty(.Ok(x, s, ParseError(position: s.position))) }
                .switchOn(self.parse(s))
        }
    }

    var ignore: Parser<T, U, Void> {
        return self <^> const(())
    }

    var many: Parser<T, U, [A]> {
        return reduce([], combine: uncurry(Array.cappend))
    }

    var many1: Parser<T, U, [A]> {
        return self >>- { self.reduce([$0], combine: uncurry(Array.cappend)) }
    }

    var skipMany: Parser<T, U, Void> {
        return reduce((), combine: const(()))
    }

    var skipMany1: Parser<T, U, Void> {
        return self >>- const(skipMany)
    }

    func reduce<B>(first: B, combine: (B, A) -> B) -> Parser<T, U, B> {
        return Parser<T, U, B> { (var s) in
            var accum = first

            while true {
                switch self.parse(s) {
                    case .Eaten(let .Ok(v, s2, _)):
                        s = s2
                        accum = combine(accum, v)
                        continue

                    case .Eaten(let .Error(e)):
                        return .Eaten(.Error(e))

                    case .Empty(let .Error(e)):
                        return .Eaten(.Ok(accum, s, e))

                    case .Empty(.Ok):
                        preconditionFailure("combinator 'reduce' is applied to a non-consuming parser")
                }
            }
        }
    }

    func reduce1<B>(first: B, combine: (B, A) -> B) -> Parser<T, U, B> {
        return self >>- { self.reduce(combine(first, $0), combine: combine) }
    }
}

// MARK: - Handling state

private func updateParserState<T, U, A>(f: (inout State<T, U>) -> A) -> Parser<T, U, A> {
    return Parser { (var s) in
        let val = f(&s)
        return .Empty(.Ok(val, s, ParseError(position: s.position)))
    }
}

public func setParserState<T, U>(state: U) -> Parser<T, U, Void> {
    return modifyState(const(state))
}

public func getParserState<T, U>() -> Parser<T, U, U> {
    return modifyState { (inout user: U) in user }
}

public func modifyState<T, U>(f: U -> U) -> Parser<T, U, Void> {
    return updateParserState { (inout s: State<T, U>) in s.user = f(s.user) }
}

public func modifyState<T, U, A>(f: (inout U) -> A) -> Parser<T, U, A> {
    return updateParserState { (inout s: State<T, U>) in f(&s.user) }
}

public func getParserLocation<T, U>() -> Parser<T, U, SourcePos> {
    return updateParserState { (inout s: State<T, U>) in s.position }
}

// MARK: - Private Helpers

private func makeUnexpectedError<T, U, A>(position: SourcePos, _ message: String = "") -> Reply<T, U, A> {
    return .Error(ParseError(position: position, messages: [.SysUnexpected(message)]))
}

private func mkString<T>(x: T) -> String {
    return String(x)
}
