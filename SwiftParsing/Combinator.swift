//
//  Combinator.swift
//  SwiftParsing
//
//  Created by Janek Spaderna on 09.05.15.
//  Copyright (c) 2015 Janek Spaderna. All rights reserved.
//

import Foundation
import SwiftBase

public protocol ParserType {
    typealias Value
    typealias Tokens
    typealias User

    init()
    func <|>(a: Self, b: Self) -> Self
    func >>-<B>(p: Self, f: Value -> Parser<Tokens, User, B>) -> Parser<Tokens, User, B>
    func <^><B>(p: Self, f: Value -> B) -> Parser<Tokens, User, B>
}

extension Parser: ParserType {
    public typealias Value = A
    public typealias Tokens = T
    public typealias User = U
}

public extension SequenceType where Generator.Element: ParserType {

    var choice: Generator.Element {
        return reduce(Generator.Element(), combine: <|>)
    }

    var choiceUnsafe: Generator.Element {
        return reduce1(<|>)
    }

    var sequence: Parser<Generator.Element.Tokens, Generator.Element.User, [Generator.Element.Value]> {
        return reduce(Parser(pure: [])) { p_xs, p_x in
            p_x  >>- { x in
            p_xs <^> (uncurry(Array.cappend) » x) }
        }
    }
}

public extension Parser {
    func option(fallback: A) -> Parser<T, U, A> {
        return self <|> Parser(pure: fallback)
    }

    var maybe: Parser<T, U, A?> {
        return self <^> pure <|> pure(nil)
    }

    var optional: Parser<T, U, Void> {
        return self <^> const(()) <|> pure(())
    }

    func between<Open, Close>(open: Parser<T, U, Open>, _ close: Parser<T, U, Close>) -> Parser<T, U, A> {
        return open  >>- { _ in
               self  >>- { x in
               close <^> const(x) } }
    }

    func sepBy<Sep>(sep: Parser<T, U, Sep>) -> Parser<T, U, [A]> {
        return sepBy1(sep) <|> pure([])
    }

    func sepBy1<Sep>(sep: Parser<T, U, Sep>) -> Parser<T, U, [A]> {
        return self                    >>- { x in
            (sep >>- const(self)).many <^> { [x] + $0 } }
    }

    public func sepTryEndBy<Sep>(sep: Parser<T, U, Sep>) -> Parser<T, U, [A]> {
        return sepTryEndBy1(sep) <|> pure([])
    }

    public func sepTryEndBy1<Sep>(sep: Parser<T, U, Sep>) -> Parser<T, U, [A]> {
        return self >>- { x in
                sep >>- { _ in
                    self.sepTryEndBy(sep) <^> { [x] + $0 }
                } <|> pure([x])
        }
    }

    func sepEndBy<Sep>(sep: Parser<T, U, Sep>) -> Parser<T, U, [A]> {
        return (self >>- { sep <^> const($0) }).many
    }

    func sepEndBy1<Sep>(sep: Parser<T, U, Sep>) -> Parser<T, U, [A]> {
        return (self >>- { sep <^> const($0) }).many1
    }

    func count(n: Int) -> Parser<T, U, [A]> {
        return NGeneratorOfOne(count: n, value: self).sequence
    }

    func count<I: IntervalType where I.Bound == Int>(cnt: I) -> Parser<T, U, [A]> {
        // We could optimize this by using a custom-built Parser with some sort of loop
        precondition(cnt.start >= 0)

        func scan(remaining: Int, _ accum: [A]) -> Parser<T, U, [A]> {
            return remaining == 0
                ? pure(accum)
                : (self >>- { scan(remaining - 1, accum + [$0]) } <|> pure(accum))
        }

        return count(cnt.start) >>- scan « (cnt.end - cnt.start)
    }

    func chainl(op: Parser<T, U, (A, A) -> A>)(_ x: A) -> Parser<T, U, A> {
        return chainl1(op) <|> pure(x)
    }

    func chainl1(op: Parser<T, U, (A, A) -> A>) -> Parser<T, U, A> {
        func rest(x: A) -> Parser {
            return op   >>- { f in
                   self >>- { y in rest(f(x, y)) } }
                   <|> pure(x)
        }

        return self >>- rest
    }

    func chainr(op: Parser<T, U, (A, A) -> A>)(_ x: A) -> Parser<T, U, A> {
        return chainr1(op) <|> pure(x)
    }

    func chainr1(op: Parser<T, U, (A, A) -> A>) -> Parser<T, U, A> {
        func rest(x: A) -> Parser {
            return op               >>- { f in
                   self.chainr1(op) <^> (f « x) }
                   <|> pure(x)
        }

        return self >>- rest
    }

    static var eof: Parser<T, U, Void> {
        return item(fst).notFollowing <?> "end of input"
    }

    var notFollowing: Parser<T, U, Void> {
        return (
            clean >>- { Parser<T, U, Void>(unexpected: String($0)) }
                  <|> pure(())
        ).clean
    }

    func manyTill<End>(end: Parser<T, U, End>) -> Parser<T, U, [A]> {
        func scan_many(accum: [A]) -> Parser<T, U, [A]> {
            return end <^> const(accum)
               <|> (self <^> accum.cappend) >>- scan_many
        }

        return scan_many([])
    }
}
