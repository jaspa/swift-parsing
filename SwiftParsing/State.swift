//
//  State.swift
//  SwiftParsing
//
//  Created by Janek Spaderna on 02.05.15.
//  Copyright (c) 2015 Janek Spaderna. All rights reserved.
//

import Foundation
import SwiftBase

enum Consumed<A> {
    case Eaten(A)
    case Empty(A)

    var value: A {
        switch self {
        case let .Eaten(v): return v
        case let .Empty(v): return v
        }
    }
}

enum Reply<T, U, A> {
    case Ok(A, State<T, U>, ParseError)
    case Error(ParseError)
}

struct State<T, U> {
    var input: CollectionGenerator<T>
    var position: SourcePos
    var user: U

    init(input: CollectionGenerator<T>, position: SourcePos, user: U) {
        self.input = input
        self.position = position
        self.user = user
    }

    init<C: CollectionType where C.Generator.Element == T>(input: C, position: SourcePos, user: U) {
        self.input = CollectionGenerator(input)
        self.position = position
        self.user = user
    }
}

// MARK: Typeclasses

extension Reply {
    func map<B>(@noescape f: A -> B) -> Reply<T, U, B> {
        switch self {
            case let .Ok(v, s, e): return .Ok(f(v), s, e)
            case let .Error(e):    return .Error(e)
        }
    }
}

func <^><T, U, A, B>(x: Consumed<Reply<T, U, A>>, @noescape f: A -> B) -> Consumed<Reply<T, U, B>> {
    switch x {
    case let .Eaten(v): return .Eaten(v.map(f))
    case let .Empty(v): return .Empty(v.map(f))
    }
}
