//
//  ParseError.swift
//  SwiftParsing
//
//  Created by Janek Spaderna on 26.04.15.
//  Copyright (c) 2015 Janek Spaderna. All rights reserved.
//

import Foundation
import SwiftBase

enum Message {
    case SysUnexpected(String)  // library generated unexpect
    case Unexpected   (String)  // unexpected something
    case Expected     (String)  // expected something
    case Message      (String)  // raw message

    private var message: String {
        switch self {
            case let .SysUnexpected(msg): return msg
            case let .Unexpected   (msg): return msg
            case let .Expected     (msg): return msg
            case let .Message      (msg): return msg
        }
    }
}

private extension Message /* : Comparable */ {
    var index: Int {
        switch self {
            case .SysUnexpected: return 0
            case .Unexpected   : return 1
            case .Expected     : return 2
            case .Message      : return 3
        }
    }
}

private func ==(lhs: Message, rhs: Message) -> Bool { return lhs.index == rhs.index }
private func !=(lhs: Message, rhs: Message) -> Bool { return lhs.index != rhs.index }
private func <(lhs: Message, rhs: Message) -> Bool { return lhs.index < rhs.index }

private extension SequenceType where Generator.Element == String {
    var cleaned: [String] {
        return filter { !$0.isEmpty }.unique()
    }
}

public struct ErrorFormatter {
    public var orString: String = ""
    public var unknownString: String = ""
    public var expectedString: String = ""
    public var unexpectedString: String = ""
    public var endOfInputString: String = ""

    public static var english: ErrorFormatter {
        return ErrorFormatter(
            orString: "or", unknownString: "unknown", expectedString: "expecting",
            unexpectedString: "unexpected", endOfInputString: "end of input")
    }

    private func format(messages: [Message]) -> String {
        if messages.isEmpty {
            return unknownString
        }

        let sysUnexpected: [Message], unexpected: [Message], expected: [Message]
        var rest: [Message]

        (sysUnexpected, rest) = messages.sort(<).span((==) « .SysUnexpected(""))
        (unexpected, rest) = rest.span((==) « .Unexpected(""))
        (expected, rest) = rest.span((==) « .Expected(""))

        let sysUnexpectedString: String

        if unexpected.isEmpty, let firstMsg = sysUnexpected.first?.message {
            sysUnexpectedString = unexpectedString + " " + (firstMsg.isEmpty ? endOfInputString : firstMsg)
        } else {
            sysUnexpectedString = ""
        }

        return [
            sysUnexpectedString,
            show(unexpected, named: unexpectedString),
            show(expected, named: expectedString),
            show(rest)
        ].cleaned.joinWithSeparator("\n")
    }

    private func show<S: SequenceType where S.Generator.Element == Message>(msgs: S, named pre: String = "") -> String {
        let msgStrings = msgs.lazy.map { $0.message }.cleaned
        if msgStrings.isEmpty { return "" }
        let (msgs, last) = msgStrings.tail
        let joinedMsgs = msgs.isEmpty ? last : msgs.joinWithSeparator(", ") + " \(orString) \(last)"
        return pre.isEmpty ? joinedMsgs : pre + " " + joinedMsgs
    }
}

public struct ParseError {
    let position: SourcePos
    let messages: [Message]
    var isUnknown: Bool { return messages.isEmpty }

    init(position: SourcePos, messages: [Message] = []) {
        self.position = position
        self.messages = messages
    }

    func mergeWith(error: ParseError) -> ParseError {
        if (isUnknown && !error.isUnknown) {
            return error
        }

        if (!isUnknown && error.isUnknown) {
            return self
        }

        if (position < error.position) {
            return error
        }

        if (position > error.position) {
            return self
        }

        return ParseError(position: position, messages: messages + error.messages)
    }

    func addMessage(msg: Message) -> ParseError {
        return ParseError(position: position, messages: messages + [msg])
    }

    func setMessage(msg: Message) -> ParseError {
        return ParseError(position: position, messages: messages.filter((!=) « msg) + [msg])
    }
}

extension ParseError: CustomStringConvertible {
    public var description: String {
        return show()
    }

    public func show(formatter: ErrorFormatter = .english) -> String {
        return position.description + ":\n" + formatter.format(messages)
    }
}

// Required for testing
public extension ParseError {
    func errorsInSpecification(spec: [String: AnyObject]) -> [String] {
        var errors: [String] = []

        if let line = spec["line"] as? Int where position.line != line {
            errors.append("line spec doesn't match: expected \(line) – got \(position.line)")
        }

        if let char = (spec["char"] ?? spec["character"]) as? Int where position.char != char {
            errors.append("char spec doesn't match: expected \(char) – got \(position.char)")
        }

        if let specErrs = spec["errors"] as? [String] {
            var msgStrings = description.componentsSeparatedByString("\n")[1..]

            for err in specErrs {
                if let errIdx = msgStrings.indexOf(err) {
                    msgStrings.removeAtIndex(errIdx)
                } else {
                    errors.append("error spec not found: " + err)
                }
            }

            errors.appendContentsOf(msgStrings.map((+) « "unexpected error: "))
        }

        return errors
    }
}
