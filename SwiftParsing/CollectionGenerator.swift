//
//  CollectionGenerator.swift
//  SwiftParsing
//
//  Created by Janek Spaderna on 10.05.15.
//  Copyright (c) 2015 Janek Spaderna. All rights reserved.
//

import Foundation

private final class Box<T> {
    let value: T
    init(_ value: T) { self.value = value }
}

struct CollectionGenerator<T>: GeneratorType, SequenceType {
    private var _idx: AnyObject
    private let _next: AnyObject -> (AnyObject, T)?

    init<C: CollectionType where C.Generator.Element == T>(_ list: C) {
        _idx = Box(list.startIndex)
        _next = { idxobject in
            let idxbox: Box<C.Index> = unsafeDowncast(idxobject), idx = idxbox.value
            return idx != list.endIndex ? (Box(idx.successor()), list[idx]) : nil
        }
    }

    func generate() -> CollectionGenerator {
        return self
    }

    mutating func next() -> T? {
        if let (newidx, elem) = _next(_idx) {
            _idx = newidx
            return elem
        }

        return nil
    }
}
