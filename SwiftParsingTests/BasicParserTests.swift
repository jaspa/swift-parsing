//
//  BasicParserTests.swift
//  SwiftParsing
//
//  Created by Janek Spaderna on 03.05.15.
//  Copyright (c) 2015 Janek Spaderna. All rights reserved.
//

import Cocoa
import XCTest
import SwiftParsing
import SwiftBase
import SwPChar

func ==(lhs: (), rhs: ()) -> Bool { return true }

class BasicParserTests: XCTestCase {

    func testPure() {
        CheckParseResult(Parser(pure: 1), [1,2], (1, [1,2]))
    }

    func testMap() {
        CheckParseResult(Parser(pure: 1) <^> ((+) « 1), [1,2], (2, [1,2]))
    }

    func testBind() {
        CheckParseResult(Parser(pure: 1) >>- { _ in pure(2) }, [1,2], (2, [1,2]))
    }

    func testFail() {
        CheckParseError(Parser(fail: "foo") as Parser<Int, Void, Void>, [], ["errors": ["foo"]])
        CheckParseError(Parser(unexpected: "foo") as Parser<Int, Void, Void>, [], ["errors": ["unexpected foo"]])
    }

    func testMany() {
        var p: Parser<Int, Void, [Int]> = item { s, _ in s.increaseChar() }.many
        CheckParseResult(p, [1,2,3], ([1,2,3], []), on: (==))
        CheckParseResult(p, [],      ([], []),      on: (==))

        p = item { s, _ in s.increaseChar() }.many1
        CheckParseResult(p, [1,2,3], ([1,2,3], []), on: (==))
        CheckParseError (p, [],      nil)
    }

    func testSkipMany() {
        CheckParseResult(item { s, _ in s.increaseChar() }.skipMany, [1,2,3], ((), []), on: (==))
        CheckParseResult(item { s, _ in s.increaseChar() }.skipMany, [Int](), ((), []), on: (==))
    }

    func testLookahead() {
        CheckParseResult(item { s, _ in s.increaseChar() }.lookAhead, [1,2,3], (1, [1,2,3]), on: (==))
    }

    func testManyTill() {
        let p: Parser<Character, Void, [Int]> = (oneOf("1234567890") <^> { Int(String($0))! }).manyTill(char("!"))
        CheckParseResult(p, ["1", "2", "!"], ([1,2], []), on: (==))
        CheckParseError(p, ["1", "2"], ["errors": ["unexpected end of input", "expecting '!'"]])
    }

    func testEOF() {
        CheckParseResult(.eof, [] as [Int], ((), []), on: (==))
        CheckParseError(.eof, [1], ["errors": ["unexpected 1", "expecting end of input"]])
    }

    func testChoice() {
        let p: GenCharParser<Void>.T = char("a") <|> char("b")
        CheckParseResult(p, ["a"], ("a", []))
        CheckParseResult(p, ["b"], ("b", []))
        CheckParseError(p, [], ["errors": ["unexpected end of input", "expecting 'a' or 'b'"]])
    }

    func testChoiceUnsafeSequence() {
        let choices: GenCharParser<Void>.T = [char("a"), char("b"), char("c")].choiceUnsafe
        CheckParseResult(choices, ["a"], ("a", []))
        CheckParseResult(choices, ["b"], ("b", []))
        CheckParseResult(choices, ["c"], ("c", []))
        CheckParseError(choices, [], ["errors": ["unexpected end of input", "expecting 'a', 'b' or 'c'"]])
    }
}
