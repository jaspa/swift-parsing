//
//  TestingHelpers.swift
//  SwiftParsing
//
//  Created by Janek Spaderna on 03.05.15.
//  Copyright (c) 2015 Janek Spaderna. All rights reserved.
//

import Foundation
import XCTest
import SwiftParsing
import SwiftBase

func CheckParseResult<T: Equatable, A: Equatable>(p: Parser<T, Void, A>, _ input: [T], _ result: (A, [T]?)?, file: String = __FILE__, line: UInt = __LINE__) {
    CheckParseResult(p, input, result, file: file, line: line) { $0 == $1 }
}

func CheckParseResult<T: Equatable, A>(p: Parser<T, Void, A>, _ input: [T], _ result: (A, [T]?)?, file: String = __FILE__, line: UInt = __LINE__, on: (A, A) -> Bool) {
    typealias SucFun = (A, [T]) -> String?
    let error: ErrFun?, success: SucFun?

    if let result = result {
        error = { "expected '\(result)' – got error\n\($0)" }
        success = { r, ts in
            if !on(result.0, r) {
                return "expected '\(result.0)' – got '\(r)'"
            } else if let rest = result.1 where ts != rest {
                return "expected rest '\(rest)' – got '\(ts)'"
            }; return nil
        }
    } else {
        error = nil
        success = unexpectedMatch
    }

    DoCheck(p.run(input, source: "", user: ()), info: CheckInfo(file, line), onError: error, onSuccess: success)
}

func CheckParseError<A, T>(p: Parser<T, Void, A>, _ input: [T], _ result: [String: AnyObject]?, file: String = __FILE__, line: UInt = __LINE__) {
    let error: ErrFun? = result.map { result in
        { err in
            let errors = err.errorsInSpecification(result)
            return errors.isEmpty ? nil : "got error:\n\(err)\n" + errors.joinWithSeparator("\n")
        }
    }

    DoCheck(p.run(input, source: "", user: ()), info: CheckInfo(file, line), onError: error, onSuccess: unexpectedMatch)
}

// MARK: - Private Helpers

private typealias ErrFun = ParseError -> String?

private struct CheckInfo {
    let f: String
    let l: UInt

    init(_ F: String, _ L: UInt) {
        f = F; l = L
    }
}

private func unexpectedMatch<T, A>(match: A, _: [T]) -> String! {
    return "expected error – got match '\(match)'"
}

private func DoCheck<T, A>(@autoclosure result: () -> Either<ParseError, (A, AnyGenerator<T>)>, info: CheckInfo, onError: ErrFun?, onSuccess: ((A, [T]) -> String?)?) {
    switch result() {
        case let .Left(val):
            if let error = onError?(val) {
                XCTFail(error, file: info.f, line: info.l)
            }

        case let .Right(val):
            let (result, tokens) = val
            if let error = onSuccess?(result, Array(tokens)) {
                XCTFail(error, file: info.f, line: info.l)
            }
    }
}
